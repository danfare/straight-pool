package MojoFull::Games;
use Mojo::Base 'Mojolicious::Controller';
use Data::Dumper;

sub index {
  my $self = shift;

  my @games = $self->db->resultset('Game')->latest;
  $self->stash(games => \@games);

  $self->render('games/index');
}

sub show {
  my $self  = shift;
  my $param = $self->stash('id');

  my $game = $self->db->resultset('Game')->by_id($param)
    or return $self->redirect_to('games');

  my $data = [];
  for my $player_num (1..$game->players) {
    # get rack data
    my @rdata;
    my @racks = $self->db->resultset('RackPlayer')->racks_by_player($game->game_id, $player_num);
    for my $rack (@racks) {
      my $hash = {
        game_id => $game->game_id,
        rack_id => $rack->rack_id,
        rack_player_id => $rack->rack_player_id,
        balls => $rack->balls,
        fouls => $rack->fouls,
        player => $rack->player
      };
      push @rdata, $hash
    }

    push @$data, \@rdata;
  }

  $self->stash(game => $game);
  $self->stash(rack_data => $data);

  $self->render('games/show2');
}

sub create {
  my $self  = shift;

  my $result = $self->db->resultset('Game')->new({
    players => $self->param("num_of_players"),
    created_at => \'NOW()'
  });
  $result->insert;

  $self->redirect_to('games/' . $result->game_id);
}

sub create_rack {
  my $self = shift;


  my $game_id = $self->stash('id');

  my $game = $self->db->resultset('Game')->by_id($game_id);

  # create rack row
  my $result = $self->db->resultset('Rack')->new({
    game_id => $game_id,
    created_at => \'NOW()'
  });

  $result->insert;

  say 'new rack id';
  say $result->rack_id;

  for my $player_num (1..$game->players) {
      my $rp = $self->db->resultset('RackPlayer')->new({
        rack_id => $result->rack_id,
        player => $player_num,
        created_at => \'NOW()'
      });
      $rp->insert;
  }

  $self->render(json => { rack_id => $result->rack_id });
}

sub update_player_rack {
  my $self = shift;

  # rack_player.rack_player_id
  my $rp_id = $self->param('rp_id');

  say Dumper($self);

  my $rp = $self->db->resultset('RackPlayer')->find({ rack_player_id => $rp_id });

  my $balls = $self->req->body_params->param("balls") >= 0 ? $self->req->body_params->param("balls") : 0;
  my $fouls = $self->req->body_params->param("fouls") >= 0 ? $self->req->body_params->param("fouls") : 0;

  $rp->balls($balls);
  $rp->fouls($fouls);

  $rp->update;

  $self->render(json => {
    rack_player_id => $rp_id,
    player => $rp->player,
    balls => $rp->balls,
    fouls => $rp->fouls
  });
}

1;
