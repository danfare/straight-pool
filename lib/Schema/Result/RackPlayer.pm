use utf8;
package Schema::Result::RackPlayer;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Schema::Result::RackPlayer

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime");

=head1 TABLE: C<rack_player>

=cut

__PACKAGE__->table("rack_player");

=head1 ACCESSORS

=head2 rack_player_id

  data_type: 'integer'
  extra: {unsigned => 1}
  is_auto_increment: 1
  is_nullable: 0

=head2 rack_id

  data_type: 'integer'
  extra: {unsigned => 1}
  is_foreign_key: 1
  is_nullable: 0

=head2 player

  data_type: 'integer'
  is_nullable: 0

=head2 balls

  data_type: 'integer'
  default_value: 0
  extra: {unsigned => 1}
  is_nullable: 1

=head2 fouls

  data_type: 'integer'
  default_value: 0
  extra: {unsigned => 1}
  is_nullable: 1

=head2 created_at

  data_type: 'datetime'
  datetime_undef_if_invalid: 1
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "rack_player_id",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_auto_increment => 1,
    is_nullable => 0,
  },
  "rack_id",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 0,
  },
  "player",
  { data_type => "integer", is_nullable => 0 },
  "balls",
  {
    data_type => "integer",
    default_value => 0,
    extra => { unsigned => 1 },
    is_nullable => 1,
  },
  "fouls",
  {
    data_type => "integer",
    default_value => 0,
    extra => { unsigned => 1 },
    is_nullable => 1,
  },
  "created_at",
  {
    data_type => "datetime",
    datetime_undef_if_invalid => 1,
    is_nullable => 0,
  },
);

=head1 PRIMARY KEY

=over 4

=item * L</rack_player_id>

=back

=cut

__PACKAGE__->set_primary_key("rack_player_id");

=head1 RELATIONS

=head2 rack

Type: belongs_to

Related object: L<Schema::Result::Rack>

=cut

__PACKAGE__->belongs_to(
  "rack",
  "Schema::Result::Rack",
  { rack_id => "rack_id" },
  { is_deferrable => 1, on_delete => "NO ACTION", on_update => "NO ACTION" },
);


# Created by DBIx::Class::Schema::Loader v0.07039 @ 2018-09-13 19:01:14
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:Y4FiVi5+/uZ6oYArUiWxvA


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
