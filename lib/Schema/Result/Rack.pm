use utf8;
package Schema::Result::Rack;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Schema::Result::Rack

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime");

=head1 TABLE: C<rack>

=cut

__PACKAGE__->table("rack");

=head1 ACCESSORS

=head2 rack_id

  data_type: 'integer'
  extra: {unsigned => 1}
  is_auto_increment: 1
  is_nullable: 0

=head2 game_id

  data_type: 'integer'
  extra: {unsigned => 1}
  is_foreign_key: 1
  is_nullable: 0

=head2 created_at

  data_type: 'datetime'
  datetime_undef_if_invalid: 1
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "rack_id",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_auto_increment => 1,
    is_nullable => 0,
  },
  "game_id",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 0,
  },
  "created_at",
  {
    data_type => "datetime",
    datetime_undef_if_invalid => 1,
    is_nullable => 0,
  },
);

=head1 PRIMARY KEY

=over 4

=item * L</rack_id>

=back

=cut

__PACKAGE__->set_primary_key("rack_id");

=head1 RELATIONS

=head2 game

Type: belongs_to

Related object: L<Schema::Result::Game>

=cut

__PACKAGE__->belongs_to(
  "game",
  "Schema::Result::Game",
  { game_id => "game_id" },
  { is_deferrable => 1, on_delete => "NO ACTION", on_update => "NO ACTION" },
);

=head2 rack_players

Type: has_many

Related object: L<Schema::Result::RackPlayer>

=cut

__PACKAGE__->has_many(
  "rack_players",
  "Schema::Result::RackPlayer",
  { "foreign.rack_id" => "self.rack_id" },
  { cascade_copy => 0, cascade_delete => 0 },
);


# Created by DBIx::Class::Schema::Loader v0.07039 @ 2018-09-13 16:16:40
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:G0I7jwjsYvr7uqH2Co2FSg


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
