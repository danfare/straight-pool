use utf8;
package Schema::Result::Game;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Schema::Result::Game

=cut

use warnings;

use base 'DBIx::Class::Core';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime");

=head1 TABLE: C<game>

=cut

__PACKAGE__->table("game");

=head1 ACCESSORS

=head2 game_id

  data_type: 'integer'
  extra: {unsigned => 1}
  is_auto_increment: 1
  is_nullable: 0

=head2 players

  data_type: 'integer'
  is_nullable: 0

=head2 created_at

  data_type: 'datetime'
  datetime_undef_if_invalid: 1
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "game_id",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_auto_increment => 1,
    is_nullable => 0,
  },
  "players",
  { data_type => "integer", is_nullable => 0 },
  "created_at",
  {
    data_type => "datetime",
    datetime_undef_if_invalid => 1,
    is_nullable => 0,
  },
);

=head1 PRIMARY KEY

=over 4

=item * L</game_id>

=back

=cut

__PACKAGE__->set_primary_key("game_id");

=head1 RELATIONS

=head2 racks

Type: has_many

Related object: L<Schema::Result::Rack>

=cut

__PACKAGE__->has_many(
  "racks",
  "Schema::Result::Rack",
  { "foreign.game_id" => "self.game_id" },
  { cascade_copy => 0, cascade_delete => 0 },
);


# Created by DBIx::Class::Schema::Loader v0.07039 @ 2018-09-13 16:16:40
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:w1zmt82cGgwIWWEU4SLiCA


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
