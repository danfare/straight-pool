package MojoFull;
use Mojo::Base 'Mojolicious';
use Schema;

# Connects once for entire application. For real apps, consider using a helper
# that can reconnect on each request if necessary.
has schema => sub {
  return Schema->connect('DBI:mysql:database=dan;host=127.0.0.1;port=3306', 'nobody', 'p@ssw0rd');
};

# dbicdump -Ilib -o dump_directory=./lib       -o components='["InflateColumn::DateTime"]'       -o preserve_case=1       Schema 'DBI:mysql:database=dan;host=127.0.0.1;port=3306' nobody p@ssw0rd       '{ quote_char => "`" }'

# This method will run once at server start
sub startup {
  my $self = shift;

  $self->helper(db => sub { $self->app->schema });

  # Routes
  my $r = $self->routes;

  # Requested id is a photoset?
  $r->add_condition(
    photoset => sub {
      my ($r, $c, $captures, $pattern) = @_;

      my $id = $captures->{id};
      return 1 if $id !~ /^\d+$/ or $id =~ /^\d+$/ and length $id == 17;
    }
  );

  $r->get('/')->to('games#index');
  $r->post('/games')->to('games#create');
  $r->get('/games')->to('games#index');
  $r->get('/games/:id')->to('games#show');


  # this creates rack and rack_player records based on game.players
  $r->post('/games/:id/racks')->to('games#create_rack');
  $r->patch('/games/rack-players/:rp_id')->to('games#update_player_rack');
}

1;
